# CoAP communication

Simple setup to communicate data from the [pairing-contiki](https://gitlab.com/jorritolthuis/pairing-contiki) project.

This setup is used to measure power and time of the communication of pairing related data.

**NOTE: The IP address in the client should be set to the 'Tentative link-local IPv6 address' printed by the server (which is expected to be the same every time).**

In order to do proper timing and power measurements of the server, please replace `os/net/app-layer/coap/coap-engine.c` with [coap-engine.c](coap-engine.c).

## Architecture

```
     Server                         Client
       |                              |
   Open server                        |
       |                       Generate request
       |                 |------------|
       | <---------------|            |
 Process request                      |
       |                              |
 Generate reply                       |
       |-----------------|            |
       |                 |----------> |
       |                        Receive reply
       |                              |
 Keep listening                       X
       |
```

##

Building server (similar for client):

```
make TARGET=zoul BOARD=remote-revb POWER=1
sudo make TARGET=zoul BOARD=remote-revb POWER=1 server.upload
```


## License

This code is based on the Contiki-NG CoAP example, which has the  "BSD 3-Clause "New" or "Revised" License". For this reason, this derivative is using the same license.
