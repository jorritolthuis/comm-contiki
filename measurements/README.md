# Measurements

This directory contains various measurements. Of the CoAP communication between two nodes.

Both type `a` and `f` are tested. Communicating each of the two inputs, `i1` and `i2`, and output `o` is tested both on the `server` and `client` side.

1 through 5 on the client side may be faulty. `f-i1` and `f-i2` would be the most likely to be correct. Because of this, trials 6 through 10 are included here.

Various measurements had double entries (same checksum), these were removed (explaining the gaps in numbering). Probably due to some ctrl+c commands not being registered.
