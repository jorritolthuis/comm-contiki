/*
 * res-pairing.c
 *
 *  Created on: May 9, 2022
 *      Author: Jorrit Olthuis
 */

#include <stdlib.h>
#include <string.h>
#include "coap-engine.h"

#include "lpm.h"
#include "rtimer.h"

static void f_input1_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset);
static void f_input2_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset);
static void f_output_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset);
static void a_input1_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset);
static void a_input2_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset);
static void a_output_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset);

static void generic_get_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset, char const *const message);

RESOURCE(f_input_1,			// Variable name defined in server.c as 'extern'
         "title=\"Hello world: ?len=0..\";rt=\"Text\"", // Attributes, details unclear for now
         f_input1_handler,	// Handler for an HTTP GET request
         NULL,				// Handler for an HTTP POST request
         NULL,				// Handler for an HTTP PUT request
         NULL);				// Handler for an HTTP DELETE request

RESOURCE(f_input_2,			// Variable name defined in server.c as 'extern'
         "title=\"Hello world: ?len=0..\";rt=\"Text\"", // Attributes, details unclear for now
		 f_input2_handler,	// Handler for an HTTP GET request
         NULL,				// Handler for an HTTP POST request
         NULL,				// Handler for an HTTP PUT request
         NULL);				// Handler for an HTTP DELETE request

RESOURCE(f_output,			// Variable name defined in server.c as 'extern'
         "title=\"Hello world: ?len=0..\";rt=\"Text\"", // Attributes, details unclear for now
		 f_output_handler,	// Handler for an HTTP GET request
         NULL,				// Handler for an HTTP POST request
         NULL,				// Handler for an HTTP PUT request
         NULL);				// Handler for an HTTP DELETE request

RESOURCE(a_input_1,			// Variable name defined in server.c as 'extern'
         "title=\"Hello world: ?len=0..\";rt=\"Text\"", // Attributes, details unclear for now
		 a_input1_handler,	// Handler for an HTTP GET request
         NULL,				// Handler for an HTTP POST request
         NULL,				// Handler for an HTTP PUT request
         NULL);				// Handler for an HTTP DELETE request

RESOURCE(a_input_2,			// Variable name defined in server.c as 'extern'
         "title=\"Hello world: ?len=0..\";rt=\"Text\"", // Attributes, details unclear for now
		 a_input2_handler,	// Handler for an HTTP GET request
         NULL,				// Handler for an HTTP POST request
         NULL,				// Handler for an HTTP PUT request
         NULL);				// Handler for an HTTP DELETE request

RESOURCE(a_output,			// Variable name defined in server.c as 'extern'
         "title=\"Hello world: ?len=0..\";rt=\"Text\"", // Attributes, details unclear for now
		 a_output_handler,	// Handler for an HTTP GET request
         NULL,				// Handler for an HTTP POST request
         NULL,				// Handler for an HTTP PUT request
         NULL);				// Handler for an HTTP DELETE request

static void f_input1_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset)
{
	char const *const message =
			"017399EF4E751701A8A58D41587A";
	generic_get_handler(request, response, buffer, preferred_size, offset, message);
}

static void f_input2_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset)
{
	char const *const message =
			"002A4B2F7B154701AD4697E073FD004AD80347ED610095D26D8709D2";
	generic_get_handler(request, response, buffer, preferred_size, offset, message);
}

static void f_output_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset)
{
	char const *const message =
			"00B0D184DFBC7A01A8FD4F2B5FF20246BCC7C3749D017426E5C37C0700FF0BAD3AA0C20188DEC942AFEA00F0F2E52895DC01F1F63F0FD3BB0203AD5147D09801F6A0B7E776C601E18278F3D681010BC03090A36E";
	generic_get_handler(request, response, buffer, preferred_size, offset, message);
}

static void a_input1_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset)
{
	char const *const message =
			"17B3FC211743D1BFF8BAD73EF7D78023A093E6B39D2354E62CC2B9A41CFF5BA7A885100364634AD041F704D9AF7E7CFA6355AE518A2F88614863CC8EFCC0FFFF4568947C0F27A08731A331A5AD663A986B66BFE15A8FC9D05346076AD13C0890";
	generic_get_handler(request, response, buffer, preferred_size, offset, message);
}

static void a_input2_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset)
{
	char const *const message =
			"114CA8D4A4B99C9829907C3754D148E3228F9709A350FBC0235BD613C47E665EA69CE088B59E3F93CF6398F5A9E213418E88BC798753A6BD35FD1D2D8F4455553B7D7EF4305BECBED66F7EB8005BB5BF4936DBDDECEAB472EE9596ACB044F782";
	generic_get_handler(request, response, buffer, preferred_size, offset, message);
}

static void a_output_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer,
		uint16_t preferred_size, int32_t* offset)
{
	char const *const message =
			"E431F409117BCDBD11C7D24E0407BF5B719EF9CA30D7FF2D3EC48A24E1FCFCF0C34AE507DF9222C8DCDF8D564912B9799BB11170CC4DE426466710A2B71925F0A6B6E06BE0513D9D604E35605D8E777179F48C7276232A0149533118D70F003";
	generic_get_handler(request, response, buffer, preferred_size, offset, message);
}

static void generic_get_handler(coap_message_t* request, coap_message_t* response, uint8_t* buffer, uint16_t preferred_size,
		int32_t* offset, char const *const message)
{
	size_t length = strlen(message);

	if (length < REST_MAX_CHUNK_SIZE) {
//		printf("Message fits in one chunck, sending it in one piece.\n");

		/* The query string can be retrieved by rest_get_query() or parsed for its key-value pairs. */
		const char *len = NULL;
		if (coap_get_query_variable(request, "len", &len)) {
			length = atoi(len);
			if (length < 0) {
				length = 0;
			}
			if (length > REST_MAX_CHUNK_SIZE) {
				printf("ERROR %s:%d: REST_MAX_CHUNK_SIZE exceeded (%d)\n", __FILE__, __LINE__, length);
				length = REST_MAX_CHUNK_SIZE;
			}
			memcpy(buffer, message, length);
		} else {
			memcpy(buffer, message, length);
		}

		coap_set_header_content_format(response, TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
		coap_set_header_etag(response, (uint8_t*) &length, 1);
		coap_set_payload(response, buffer, length);
	} else {
//		printf("Message is too large, sending multiple chunks... ");
		/*
		 * preferred_size 	Preferred length of available space in buffer
		 * offset 			Upon calling, equals the current offset in the full stream.
		 * 					Should be the new offset value upon returning. -1 when this transmission is the last one.
		 */

		/* Check the offset for boundaries of the resource data. */
		if(*offset >= length) {
			printf("Error\n");
			coap_set_status_code(response, BAD_OPTION_4_02);
			const char *error_msg = "BlockOutOfScope";
			coap_set_payload(response, error_msg, strlen(error_msg));
			return;
		}

		if (*offset + preferred_size >= length) {
			/* Last transmission */
//			printf("Last transmission.\n");
			size_t remaining_length = length - *offset;
			memcpy(buffer, message + *offset, remaining_length);
			coap_set_payload(response, buffer, remaining_length);
			*offset = -1; /* Indicate last transmission */
		} else {
			/* More transmissions to come */
//			printf("More transmissions to come.\n");
			memcpy(buffer, message + *offset, preferred_size);
			coap_set_payload(response, buffer, preferred_size);
			*offset += preferred_size;
		}
	}
}
