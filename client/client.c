/*
 * Copyright (c) 2013, Institute for Pervasive Computing, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      Erbium (Er) CoAP client example.
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "coap-engine.h"
#include "coap-blocking-api.h"

/* Log configuration */
#include "coap-log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL  LOG_LEVEL_APP

#define POWER

#ifdef POWER
#include "rgb-led.h"
#include "rtimer.h"
void rtimer_callback(struct rtimer* timer, void* ptr) {}
static struct rtimer timer_rtimer;
#endif /* POWER */

/* FIXME: This server address is hard-coded for Cooja and link-local for unconnected border router. */
#define SERVER_EP "coap://[fe80::212:4B00:1932:E266]"//"coap://[fe80::212:7402:0002:0202]"

PROCESS(client, "Pairing Communication Client");
AUTOSTART_PROCESSES(&client);

char* url = "hw";
//char* url = "f/i1";
//char* url = "f/i2";
//char* url = "f/o";
//char* url = "a/i1";
//char* url = "a/i2";
//char* url = "a/o";

int received = 50;

/* This function is will be passed to COAP_BLOCKING_REQUEST() to handle responses. */
void
client_chunk_handler(coap_message_t *response)
{
  const uint8_t *data;

  if(response == NULL) {
    printf("Request timed out");
    return;
  }

  /*int len = */coap_get_payload(response, &data);

  received--;

#ifdef POWER
  if (!received) {
    rgb_led_set(RGB_LED_WHITE);
  }
#endif /* POWER */
//  printf("|%.*s", len, (char *)data);
}

PROCESS_THREAD(client, ev, data)
{
  static coap_endpoint_t server_ep;
  PROCESS_BEGIN();

#ifdef POWER
  rtimer_set(&timer_rtimer, RTIMER_NOW() + RTIMER_SECOND, 0,
	 rtimer_callback, NULL);
  do {} while (rtimer_arch_next_trigger() > RTIMER_NOW());
    rgb_led_set(RGB_LED_WHITE);
#endif /* POWER */

  static coap_message_t request[1];      /* This way the packet can be treated as pointer as usual. */

  coap_endpoint_parse(SERVER_EP, strlen(SERVER_EP), &server_ep);

  /* Start main transmission */
#define REQUEST coap_init_message(request, COAP_TYPE_CON, COAP_GET, 0);\
  coap_set_header_uri_path(request, url);\
  COAP_BLOCKING_REQUEST(&server_ep, request, client_chunk_handler);

  printf("--Requesting %s--\n", url);
  LOG_INFO_COAP_EP(&server_ep);
  LOG_INFO_("\n");

#ifdef POWER
	rgb_led_off();
#endif /* POWER */

  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST

  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST

  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST

  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST

  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST
  REQUEST

  /* End main transmission */

  PROCESS_END();
}
